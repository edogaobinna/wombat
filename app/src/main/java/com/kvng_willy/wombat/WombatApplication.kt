package com.kvng_willy.wombat

import androidx.multidex.MultiDexApplication
import com.kvng_willy.wombat.data.network.MyApi
import com.kvng_willy.wombat.data.network.NetworkConnection
import com.kvng_willy.wombat.data.repository.DataRepository
import com.kvng_willy.wombat.ui.ViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class WombatApplication:MultiDexApplication(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@WombatApplication))
        bind() from singleton { NetworkConnection(instance()) }
        bind() from singleton { MyApi(instance()) }
        bind() from singleton { DataRepository(instance()) }
        bind() from provider { ViewModelFactory(instance()) }
    }

}