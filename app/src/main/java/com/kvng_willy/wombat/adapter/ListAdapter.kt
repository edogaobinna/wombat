package com.kvng_willy.wombat.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.kvng_willy.wombat.R
import com.kvng_willy.wombat.data.Posts
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ListAdapter(private val context: Context, private val posts:ArrayList<Posts>): BaseAdapter() {
    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    override fun getCount(): Int {
        return posts.size
    }

    override fun getItem(position: Int): Any {
        return posts[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @SuppressLint("ViewHolder", "SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView = inflater.inflate(R.layout.item_post, parent, false)
        val post = getItem(position) as Posts

        val author = rowView.findViewById(R.id.author) as TextView
        val title = rowView.findViewById(R.id.title) as TextView
        val htmlText = rowView.findViewById(R.id.text) as TextView
        author.text = "Posted By: ${post.author}"
        title.text = Html.fromHtml(post.title,Html.FROM_HTML_MODE_COMPACT)
        htmlText.text = Html.fromHtml(post.htmlText,Html.FROM_HTML_MODE_LEGACY)
        return rowView
    }
}
