package com.kvng_willy.wombat.data.network

import okhttp3.*
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET


interface MyApi {
    @GET("hot.json")
    fun getRedditPost() : Call<ResponseBody>


    companion object{
        operator fun invoke(
            networkConnection: NetworkConnection
        ) : MyApi {

            /*val okkHttpclient = OkHttpClient.Builder()
                .cache(networkConnection.cache())
                .addInterceptor{ chain->
                    var request = chain.request()
                    request = if (networkConnection.isInternetAvailable())
                        request.newBuilder().header("Cache-Control", "public, max-age=" + 5)
                            .build()
                    else
                        request.newBuilder().header(
                            "Cache-Control",
                            "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7
                        )
                            .build()
                    chain.proceed(request)
                }
                .build()*/

            val okkHttpclient = OkHttpClient.Builder()
                .cache(networkConnection.cache())
                .addInterceptor { chain ->
                    var request: Request = chain.request()
                    if (!networkConnection.isInternetAvailable()) {
                        val maxStale = 60 * 60 * 24 * 7 // tolerate 4-weeks stale \
                        request = request
                            .newBuilder()
                            .header(
                                "Cache-Control",
                                "public, only-if-cached, max-stale=$maxStale"
                            )
                            .build()
                    }
                    chain.proceed(request)
                }
                .build()

            return Retrofit.Builder()
                .baseUrl("https://www.reddit.com/r/Android/")
                .client(okkHttpclient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(MyApi::class.java)
        }
    }
}