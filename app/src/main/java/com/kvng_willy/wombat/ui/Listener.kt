package com.kvng_willy.wombat.ui

import androidx.lifecycle.LiveData

interface Listener {
    fun onTaskReturn(message: LiveData<String>?)
}