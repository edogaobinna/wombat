package com.kvng_willy.wombat.ui

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.kvng_willy.wombat.R
import com.kvng_willy.wombat.adapter.ListAdapter
import com.kvng_willy.wombat.databinding.ActivityMainBinding
import com.kvng_willy.wombat.utility.hide
import com.kvng_willy.wombat.utility.toast
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class MainActivity : AppCompatActivity(),KodeinAware,Listener {

    override val kodein by kodein()

    private val factory: ViewModelFactory by instance()
    private lateinit var viewmodel: ViewModel
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewmodel = ViewModelProvider(this,factory).get(ViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        viewmodel.mainListener = this

        viewmodel.fetchTask()

    }

    override fun onTaskReturn(message: LiveData<String>?) {
        message?.observe(this, Observer {
            if(it.equals("an error occurred",ignoreCase = true)){
                binding.progressCircular.hide()
                binding.title.text = getString(R.string.title_error)
            }else{
                val data = viewmodel.processJson(it)
                val adapter = ListAdapter(this, data)
                binding.listView.adapter = adapter
                binding.logoHold.hide()

                binding.listView.setOnItemClickListener { _: AdapterView<*>, _: View, i: Int, _: Long ->
                    val webIntent: Intent = Uri.parse(data[i].url).let { webpage ->
                        Intent(Intent.ACTION_VIEW, webpage)
                    }
                    startActivity(webIntent)
                }
            }
        })

    }
}