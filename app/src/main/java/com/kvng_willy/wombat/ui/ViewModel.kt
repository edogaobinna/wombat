package com.kvng_willy.wombat.ui

import androidx.lifecycle.ViewModel
import com.kvng_willy.wombat.data.Posts
import com.kvng_willy.wombat.data.repository.DataRepository
import com.kvng_willy.wombat.utility.JsonWorker

class ViewModel(
    private val repository: DataRepository
):ViewModel() {

    var mainListener: Listener? = null

    fun fetchTask(){
        val response = repository.getStatus()
        mainListener?.onTaskReturn(response)
    }

    fun processJson(json:String):ArrayList<Posts>{
        return JsonWorker().stripJson(json)
    }

}